LaTeX style file for CSCRS.

Installation
============

Easy
----

Just copy `cscrs.sty` into the same directory as your document file.

Advanced
--------

Only do this if you know what you are doing.

Copy `cscrs.sty` to your `texmf/tex/latex/local/` directory and update with
`texhash`

Usage
=====

For `.tex` and `.Rnw` files, use the `article` class and add
`\usepackage{cscrs}` into your preamble.

Options
-------

### Titlepage ###

**RECOMMENDED** A title page can be created with the `article` class option of
`titlepage`.  Without a title page, a much simpler title is printed on the
first page.

    ::tex
    \documentclass[titlepage, 12pt]{article}

### Twosided ###

**RECOMMENDED** Two-sided nature (different headers/footers for even and odd
pages) can be enabled with the `article` class option of `twoside`.

    ::tex
    \documentclass[titlepage, twoside, 12pt]{article}

### MSU colors ###

The document can be prepared with a MSU themed colors or with all black and
white text.  Black and white is default, but color can be added with the
package option of `msucolor`

    ::tex
    \usepackage[msucolor]{cscrs}

### Client name ###

**RECOMMENDED** The name of the client can be defined in the preamble for
mention on the title page.

    ::tex
    \def\client{Client Name}

### Director name ###

**RECOMMENDED** The name of the director can be defined in the preamble for
mention on the title page.

    ::tex
    \def\director{Lillian Lin}

### Secondary statisticians ###

Any secondary statisticians on the project can be acknowledged on the title
page by defining a secondary  in the preamble.

    ::tex
    \def\secondary{Worker Bee 1 and Worker Bee 2}
